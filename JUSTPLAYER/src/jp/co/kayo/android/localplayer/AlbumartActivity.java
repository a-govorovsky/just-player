package jp.co.kayo.android.localplayer;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 *      This program is free software; you can redistribute it and/or modify it under
 *      the terms of the GNU General Public License as published by the Free Software
 *      Foundation; either version 2 of the License, or (at your option) any later
 *      version.
 *      
 *      This program is distributed in the hope that it will be useful, but WITHOUT
 *      ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *      FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *      details.
 *      
 *      You should have received a copy of the GNU General Public License along with
 *      this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *      Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioAlbum;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioArtist;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.core.ProgressDialogTask;
import jp.co.kayo.android.localplayer.core.ViewHolder;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.StrictHelper;
import jp.co.kayo.android.localplayer.util.ThemeHelper;
import jp.co.kayo.android.localplayer.util.ViewCache;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class AlbumartActivity extends ListActivity implements
        OnItemClickListener, OnClickListener {
    private final String APPHOST = "findalbumart.appspot.com";
    private final String REFERER = "http://kayosystem.blogspot.com/";

    String mAlbum = null;
    String mArtist = null;
    String mTitle = null;
    String album_art = null;
    File saveFile;

    EditText editTitle;
    EditText editArtist;
    EditText editAlbum;
    Button button01;
    int fitsize = 256;
    private HashMap<String, Bitmap> images = new HashMap<String, Bitmap>();
    private boolean isfoundItem;

    ArrayList<ImageValue> mItemlist = new ArrayList<ImageValue>();
    HashMap<String, ArrayList<ImageValue>> albummap = new HashMap<String, ArrayList<ImageValue>>();
    ImageAdapter adapter;

    Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        new ThemeHelper().selectTheme(this);
        StrictHelper.registStrictMode();
        super.onCreate(savedInstanceState);
        this.getWindow().setSoftInputMode(
                LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(jp.co.kayo.android.localplayer.R.layout.albumart);

        // listview = (ListView)findViewById(R.id.ListView01);
        adapter = new ImageAdapter(this, mItemlist);
        getListView().setAdapter(adapter);
        getListView().setOnItemClickListener(this);

        editTitle = (EditText) findViewById(R.id.editTitle);
        editArtist = (EditText) findViewById(R.id.editArtist);
        editAlbum = (EditText) findViewById(R.id.editAlbum);
        button01 = (Button) findViewById(R.id.Button01);

        button01.setEnabled(true);
        button01.setOnClickListener(this);

        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        int max = Math.max(display.getHeight(), display.getWidth());

        fitsize = max / 4;

        // IntentからMediaIDを取得する
        Intent it = getIntent();
        if (it != null) {
            String album = it.getStringExtra("album");
            if (Funcs.isNotEmpty(album)) {
                editAlbum.setText(album);
            }
            String artist = it.getStringExtra("artist");
            if (Funcs.isNotEmpty(artist)) {
                editArtist.setText(artist);
            }
            String title = it.getStringExtra("title");
            if (Funcs.isNotEmpty(title)) {
                editTitle.setText(title);
            }
        }

        searchAlbumArt();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (images.size() > 0) {
            Iterator<Map.Entry<String, Bitmap>> ite = images.entrySet()
                    .iterator();
            for (; ite.hasNext();) {
                Map.Entry<String, Bitmap> ent = ite.next();
                Bitmap bmp = ent.getValue();
                if (bmp != null && !bmp.isRecycled()) {
                    bmp.recycle();
                }
            }
        }
    }

    private void listAWS() {
        BufferedReader in = null;
        try {
            isfoundItem = false;
            // URLクラスのインスタンスを生成
            StringBuffer path = new StringBuffer("http://" + APPHOST
                    + "/findalbumart?");

            if (mAlbum != null && mAlbum.length() > 0) {
                path.append("album=").append(URLEncoder.encode(mAlbum, "UTF-8"));
            } else {
                path.append("album=");
            }
            if (mArtist != null && mArtist.length() > 0) {
                path.append("&artist=").append(
                        URLEncoder.encode(mArtist, "UTF-8"));
            } else {
                path.append("&artist=");
            }
            if (mTitle != null && mTitle.length() > 0) {
                path.append("&title=")
                        .append(URLEncoder.encode(mTitle, "UTF-8"));
            } else {
                path.append("&title=");
            }

            URL url = new URL(path.toString());

            Logger.d("MyTask.openConnection");
            // 接続します
            URLConnection con = url.openConnection();
            con.setRequestProperty("Referer", REFERER);

            // 入力ストリームを取得
            in = new BufferedReader(new InputStreamReader(con.getInputStream(),
                    "UTF-8"));

            // 一行ずつ読み込みます
            StringBuffer json = new StringBuffer();
            String line = null;
            while ((line = in.readLine()) != null) {
                // 表示します
                json.append(line);
            }

            Logger.d("json=" + json.toString());
            JSONArray array = new JSONArray(json.toString());
            if (array.length() > 0) {
                int dt = 10000 / array.length();
                // Message msg = Message.obtain();
                // msg.arg1 = array.length();
                // msg.what = SystemConsts.EVT_PEOGRESS_START;
                for (int j = 0; j < array.length(); j++) {
                    isfoundItem = true;
                    JSONObject obj = array.getJSONObject(j);

                    int pos = j;

                    int n = dt * pos;
                    ImageValue imgobj = new ImageValue();
                    try {
                        imgobj.key = obj.getString("key");
                        imgobj.type = obj.getString("type");
                        imgobj.from = obj.getString("from");
                        imgobj.publisher = obj.getString("publisher");
                        imgobj.url = obj.getString("url");
                        imgobj.width = obj.getString("width");
                        imgobj.height = obj.getString("height");
                        imgobj.title = obj.getString("title");
                        imgobj.artist = obj.getString("artist");

                        String key = Integer.toString((imgobj.title+imgobj.artist).hashCode());
                        ArrayList<ImageValue> l = albummap.get(key);
                        if (l == null) {
                            l = new ArrayList<ImageValue>();
                            albummap.put(key, l);

                            mItemlist.add(imgobj);
                        }

                        l.add(imgobj);

                        // msg = Message.obtain();
                        // msg.what = SystemConsts.EVT_PEOGRESS_SET;
                        // msg.arg2 = array.length();
                        // msg.arg1 = n;
                    } catch (JSONException e) {
                        Logger.e("getString", e);
                    }
                }
            }
        } catch (Exception e) {
            Logger.e("Mytask", e);
        } finally {
            // 入力ストリームを閉じます
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                }
            }

            handler.post(new Runnable() {
                @Override
                public void run() {
                    if(!isfoundItem){
                        Toast.makeText(AlbumartActivity.this, getString(R.string.txt_empty), Toast.LENGTH_SHORT).show();
                    }
                    adapter.notifyDataSetChanged();
                }
            });
        }
    }

    public String getSuffix(String fileName) {
        if (fileName == null)
            return null;
        int point = fileName.lastIndexOf(".");
        if (point != -1) {
            return fileName.substring(point + 1).toUpperCase();
        }
        return fileName.toUpperCase();
    }

    private boolean isEnableEdit(int id) {
        CheckBox checkbox = null;

        if (id == R.id.editTitle
                && findViewById(R.id.relativeLayoutTitle).getVisibility() == View.VISIBLE) {
            checkbox = (CheckBox) findViewById(R.id.checkTitle);
        } else if (id == R.id.editArtist
                && findViewById(R.id.relativeLayoutArtist).getVisibility() == View.VISIBLE) {
            checkbox = (CheckBox) findViewById(R.id.checkArtist);
        } else if (id == R.id.editAlbum
                && findViewById(R.id.relativeLayoutAlbum).getVisibility() == View.VISIBLE) {
            checkbox = (CheckBox) findViewById(R.id.checkAlbum);
        }

        if (checkbox != null) {
            return checkbox.isChecked();
        }

        return false;
    }

    @Override
    public void onClick(View v) {
        // アート取得処理
        searchAlbumArt();
    }
    
    private void searchAlbumArt(){
        try {
            if (isEnableEdit(R.id.editAlbum) && editAlbum.getText().length()>0) {
                mAlbum = editAlbum.getText().toString();
            } else {
                mAlbum = null;
            }
            if (isEnableEdit(R.id.editArtist) && editArtist.getText().length()>0) {
                mArtist = editArtist.getText().toString();
            } else {
                mArtist = null;
            }
            if (isEnableEdit(R.id.editTitle) && editTitle.getText().length()>0) {
                mTitle = editTitle.getText().toString();
            } else {
                mTitle = null;
            }

            if (mAlbum != null || mArtist != null || mTitle != null) {
                ProgressDialogTask task = new ProgressDialogTask(this,
                        getString(R.string.txt_loading)) {
    
                    @Override
                    protected Void doInBackground(Void... params) {
                        listAWS();
                        return null;
                    }
                };
                task.execute((Void[]) null);
            }
        } catch (Exception e) {
            Logger.e("getArt", e);
        }
    }

    public class ImageValue {
        String key;
        String type;
        String from;
        String publisher;
        String url;
        String height;
        String width;
        String title;
        String artist;
    }

    public class ImageAdapter extends ArrayAdapter<ImageValue> {
        LayoutInflater inflater;
        String lbalbum;
        String lbartist;
        String lbpublisher;

        public ImageAdapter(Context context, List<ImageValue> objects) {
            super(context, 0, objects);
            inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            lbalbum = getString(R.string.fmt_album);
            lbartist = getString(R.string.fmt_artist);
            lbpublisher = getString(R.string.fmt_publisher);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.albumartrow, null);
                holder = new ViewHolder();
                holder.setImage1((ImageView) convertView
                        .findViewById(R.id.ImageView01));
                holder.setText1((TextView) convertView
                        .findViewById(R.id.TextView01));
                holder.setText2((TextView) convertView
                        .findViewById(R.id.TextView02));
                holder.setText3((TextView) convertView
                        .findViewById(R.id.TextView03));
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            ImageValue obj = getItem(position);

            holder.getText3()
                    .setText(String.format(lbpublisher, obj.publisher));

            String key = getKey(obj.url);

            Bitmap bmp = images.get(key);
            if (bmp == null) {
                holder.getImage1().setImageResource(
                        R.drawable.brank_album_white);
                ImageLoadTask task = new ImageLoadTask(obj, holder, position);
                task.execute((Void[]) null);
            } else {
                holder.getImage1().setImageBitmap(bmp);
            }

            return convertView;
        }

        void setImage(Bitmap bmp, ImageValue obj, ViewHolder holder,
                int position) {
            ImageValue target = getItem(position);
            if (target == obj) {
                holder.getImage1().setImageBitmap(bmp);
                holder.getText1().setText(String.format(lbalbum, obj.title));
                holder.getText2().setText(String.format(lbartist, obj.artist));
            }
        }
    }

    private String getKey(String url) {
        String key = "K" + url;
        return key;
    }

    public InputStream openStream(Context context, Uri uri) {
        URL url;
        try {
            if (uri.getScheme() != null && uri.getScheme().contains("http")) {
                url = new URL(uri.toString());
                return url.openStream();
            } else {
                File f = new File(uri.toString());
                if (f.exists()) {
                    return new FileInputStream(f);
                }
            }

        } catch (Exception e) {
        }

        return null;
    }

    private Bitmap getBitmap(String str) {
        Bitmap bmp = null;
        InputStream is = null;
        try {
            // まずはオリジナルの縦と横のサイズを取得するため、
            // 画像読み込みなし(inJustDecodeBounds = true)で画像を読み込む
            BitmapFactory.Options opt = new BitmapFactory.Options();
            opt.inJustDecodeBounds = true;
            Uri url = Uri.parse(str);
            is = openStream(this, url);
            if (is != null) {
                BitmapFactory.decodeStream(is, null, opt);
                is.close();
                is = null;

                // fitsizeに合わせてX方向とY方向それぞれのinSampleSizeを求める
                int sample_x = 1 + (opt.outWidth / fitsize);
                int sample_y = 1 + (opt.outHeight / fitsize);

                // inSampleSizeを設定し実際に画像の読み込みを行う
                opt.inSampleSize = Math.max(sample_x, sample_y);
                opt.inJustDecodeBounds = false;
                opt.inPurgeable = true;
                opt.inPreferredConfig = Bitmap.Config.RGB_565;
                is = openStream(this, url);
                bmp = BitmapFactory.decodeStream(is, null, opt);
                if (bmp != null) {
                    images.put(getKey(str), bmp);
                }
            }
        } catch (Exception e) {
            Logger.e("getBitmap", e);
            bmp = null;
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                }
            }
        }

        return bmp;
    }

    @Override
    public void onItemClick(AdapterView<?> p, View view, int position, long id) {
        ImageValue obj = adapter.getItem(position);

        File cacheFile = getCacheDir();
        saveFile = new File(cacheFile, "temp" + System.nanoTime() + ".jpg");
        SaveTask task = new SaveTask(obj);
        task.execute((Void[]) null);
    }

    class SaveTask extends AsyncTask<Void, Void, Void> {
        ImageValue imgobj;

        public SaveTask(ImageValue obj) {
            imgobj = obj;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            Intent data = new Intent();
            data.putExtra("savefile", saveFile.getAbsolutePath());
            setResult(Activity.RESULT_OK, data);
            finish();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // 更新処理
            OutputStream os = null;
            InputStream is = null;
            Bitmap bmp = null;
            try {
                String key = Integer.toString((imgobj.title+imgobj.artist).hashCode());
                ArrayList<ImageValue> objlist = albummap.get(key);
                ImageValue target = objlist.get(objlist.size() - 1);

                Uri url = Uri.parse(target.url);
                is = openStream(AlbumartActivity.this, url);
                if (is != null) {
                    Logger.d(saveFile.getAbsolutePath());
                    os = new FileOutputStream(saveFile);

                    byte[] buf = new byte[256];
                    int numbytes = 0;
                    while ((numbytes = is.read(buf, 0, 256)) != -1) {
                        os.write(buf, 0, numbytes);
                    }
                }
            } catch (Exception e) {
                Logger.e("onItemClick", e);
            } finally {
                if (os != null) {
                    try {
                        os.close();
                    } catch (IOException e) {
                    }
                }

                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e) {
                    }
                }
                if (bmp != null && !bmp.isRecycled()) {
                    bmp.recycle();
                }
            }
            return null;
        }

    }

    class ImageLoadTask extends AsyncTask<Void, Void, Void> {
        ImageValue obj;
        ViewHolder holder;
        int position;

        public ImageLoadTask(ImageValue obj, ViewHolder holder, int position) {
            this.obj = obj;
            this.holder = holder;
            this.position = position;
        }

        @Override
        protected Void doInBackground(Void... params) {
            final Bitmap bmp = getBitmap(obj.url);
            handler.post(new Runnable() {

                @Override
                public void run() {
                    adapter.setImage(bmp, obj, holder, position);
                }
            });

            return null;
        }

    }

}
