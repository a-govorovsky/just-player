package jp.co.kayo.android.localplayer.appwidget;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */


import java.io.File;

import android.content.Context;
import android.content.Intent;
import android.os.Environment;

public class AppWidgetHelper {
    public static final String CALL_STOP = "MediaPlayerService.Call.Stop";
    public static final String CALL_FF = "MediaPlayerService.Call.FF";
    public static final String CALL_REW = "MediaPlayerService.Call.REW";
    public static final String CALL_SHUFFLE = "MediaPlayerService.Call.SHUFFLE";
    public static final String CALL_LOOP = "MediaPlayerService.Call.LOOP";
    public static final String CALL_UPDATEWIDGET = "MediaPlayerService.Call.UPDATEWIDGET";
    public static final String CALL_PLAY_PAUSE = "MediaPlayerService.Call.PLAY_PAUSE";

    public static final String CALL_APPWIDGET_DISPLAY = "jp.co.kayo.android.localplayer.appwidget.APPWIDGET_DISPLAY";

    public static final String JUSTPLAYER_PKGNAME = "jp.co.kayo.android.localplayer";
    public static final String JUSTPLAYER_SRV_CLASSNAME = "jp.co.kayo.android.localplayer.service.MediaPlayerService";
    public static final String JUSTPLAYER_MAIN_CLASSNAME = "jp.co.kayo.android.localplayer.MainActivity";

    public static final String KEY_DISP_ARTIST = "artist";
    public static final String KEY_DISP_TITLE = "title";
    public static final String KEY_DISP_ALBUM = "album";
    public static final String KEY_DISP_STAT = "stat";

    public static final int FLG_PLAY = 1;
    public static final int FLG_PAUSE = 1 << 1;
    public static final int FLG_STOP = 1 << 2;
    public static final int FLG_LOOP1 = 1 << 3;
    public static final int FLG_LOOP2 = 1 << 4;
    public static final int FLG_SHUFFLE = 1 << 5;
    public static final int FLG_HASNEXT = 1 << 6;
    public static final int FLG_HASFOWD = 1 << 7;
    public static final int FLG_HASLIST = 1 << 9;
    
    private static final File _rootdir = new File(Environment.getExternalStorageDirectory(), "data/jp.co.kayo.android.localplayer/cache/.albumart/");
    
    public static Integer getAlbumKey(String album, String artist){
        if(album!=null && artist!=null){
            return (album.trim()+artist.trim()).hashCode();
        }
        else if(album!=null){
            return (album.trim()).hashCode();
        }
        else if(artist!=null){
            return (artist.trim()).hashCode();
        }
        return new Integer(0);
    }
    
    public static File createAlbumArtFile(Integer key){
        File file = new File(_rootdir, key+".jpg");
        return file;
    }
    

    public static Intent createServiceIntent(Context context, String action) {
        Intent i = new Intent();
        i.setClassName(AppWidgetHelper.JUSTPLAYER_PKGNAME,
                AppWidgetHelper.JUSTPLAYER_SRV_CLASSNAME);
        i.setAction(action);
        return i;
    }

    public static Intent createActivityIntent(Context context) {
        Intent i = new Intent();
        i.setClassName(AppWidgetHelper.JUSTPLAYER_PKGNAME,
                AppWidgetHelper.JUSTPLAYER_MAIN_CLASSNAME);
        return i;
    }
}
